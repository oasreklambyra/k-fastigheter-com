export default {
  global: {
    months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
  },
  page: {
    page: {
      policy: 'Back to privacy policy',
      top: 'Back to top',
      post: {

      }
    },
    inspiration: {
      latest: 'latest articles',
      readmore: 'Show all',
      category: {
        selectAll: 'All',

        post: {
          back: 'Back',
          about: 'About us',
          other: 'Other articles for you',
          backLink: {
            article: '/en/inspiration',
            job: '/en/work-with-us',
            about: '/en/about-us'
          }
        }
      }
    }
  },
  component: {
    the: {
      header: {
        menu: 'MENU',
        search: 'Search'
      },
      footer: {
        cookies: {
          content: 'K-Fastigheter.com uses cookies to improve the website and your experience of it. All data collected is stored anonymously at our provider. If you do not accept cookies we will lack of insights on how to improce our website. If you block cookies this message will reappear.',
          url: '/cookiepolicy',
          btnText: 'Find detailed information here.'
        }
      }
    },
    base: {
      archiveFilter: {
        sort: 'Sort by',
        placeholder: 'Search our articles...',
        order: 'Latest',
        category: 'All',
        searchPage: '/inspiration/all'
      },
      financalReports: {
        title: 'Financal reports',
        docs: 'Documents from'
      },
      pressRelease: {
        title: 'Press releases',
        path: '/en/investors/press-releases/',
        pdf: 'Download PDF'
      },
      cta: {
        readmore: 'Read more',
        url: '/en/corporate-governance'
      },
      postCard: {},
      postGate: {
        btnText: 'Wanna know more?'
      },
      postList: {},
      postSlider: {
        readmore: 'Show all'
      },
      postTall: {},
      postWide: {
        readmore: 'Read more'
      }
    },
    ir: {
      back: 'Go back',
      // link
      cisionArchiveCard: {
        url: '/en/investors/press-releases/',
        pdf: 'Download PDF'
      },
      contentTypeCisionArchive: {
        type: {
          options: [
            'All press releases',
            'Regulatory press releases',
            'Other press releases'
          ]
        },
        year: {
          options: [
            'Year'
          ]
        }
      },
      contentTypeCisionContent: {
        docks: 'Documents'
      },
      contentTypeCisionStocks: {
        tab: ['Sharedata', 'Orderbook'],
        Symbol: 'Symbol',
        TradeCurrency: 'Currency',
        MarketPlace: 'Market place',
        ClosePrice1D: 'Closing price',
        LastPrice: 'Latest price',
        HighPriceYtd: 'Year high',
        LowPriceYtd: 'Year low',
        Ath: 'All time high',
        Atl: 'All time low',
        NumberOfShares: 'Number of shares',
        Buy: 'Buy',
        Volume: 'Volume',
        Sell: 'Sell'
      },
      contentTypeCisionSpecial: {
        press: {
          title: 'Latest press release',
          url: '/en/investors/press-releases',
          readmore: 'Latest press release'
        },
        reports: {
          title: 'Interim reports',
          url: '/en/investors/financial-reports',
          readmore: 'Read all interim reports'
        },
        calendar: {
          title: 'Upcoming events',
          url: '/en/investors/financial-calendar',
          readmore: 'See all upcoming events'
        }
      },
      contentTypeWordpressContent: {
        docks: 'Documents from',
        files: {
          finansiellaRapporter: {
            all: 'Financial reports from all years',
            option: 'Financial reports from '
          },
          bolagsstamma: {
            all: 'General Meetings',
            option: 'Annual General Meeting '
          }
        }

      },
      contentTypeWordpressContentFilter: {
        livein: {
          label: 'Please select the country you reside in',
          placeholder: 'Country'
        },
        isin: {
          label: 'Please select the country you are in',
          placeholder: 'Country'
        },
        confirm: 'I confirm',
        deny: 'I do not confirm'
      },
      navbar: {},
      sidebar: {
        navTitle: 'Navigation',
        contactTitle: 'Contact',
        subscribeTitle: 'Subscribe'
      },
      sidebarForm: {
        title: 'Keep updated, sign up to receive our newsletter. ',
        subTitle: 'What are your interests',
        gdpr: 'When you subscribe to our mailing list, K-Fast Holding AB, 556827-0390, will process some of your personal data, namely the information that you provide in the form above and any changes to your subscription made via the link that we will send to you. We will store this data and use it when we deliver your subscription, as well as when you access the settings for your subscription. The purpose of this processing is to provide you with the subscribed information and to administer your subscription. In order to provide the subscription, we use our data processor Cision. After signing up, you will receive an e-mail from them with a link that you must click on (within 25 days) in order to activate your subscription (or it will be deleted). <br/><br/> It is up to you if you want to consent to this processing of your personal data. You can withdraw your consent at any time (which does not affect the lawfulness of our processing before your withdrawal), by using the link that you will find in the e-mail that confirms your subscription, or in any following e-mail, or by contacting us. You can read more about how we process your personal data <a href="/integritetspolicy">here</a>.',
        gdprLabel: 'OK – I consent',
        send: 'Send',
        sent: 'Sent!',
        res: {
          good: 'Sent! Wait for confirmation Email.',
          bad: 'Something went wrong. Make sure you have written a valid Email.'
        },
        form: {
          types: [
            'Annual statement',
            'Reports',
            'Press releases'
          ],
          name: {
            placeholder: 'Enter name here...',
            label: 'Name'
          },
          phone: {
            placeholder: 'Enter phone number here...',
            label: 'Phone number'
          },
          email: {
            placeholder: 'Enter e-mail address...',
            label: 'E-mail address'
          },
          company: {
            placeholder: 'Enter company name here...',
            label: 'Company name'
          },
          lang: {
            placeholder: 'Choose language...',
            label: 'Language',
            options: ['Swedish', 'English'],
            default: 'en'
          }
        }
      },
      wordpressCalendarList: {
        title: 'Upcoming events',
        order: 'Events from'
      },
      wordpressPepoleList: {}
    }
  }

}

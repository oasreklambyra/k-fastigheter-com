export default {
  global: {
    months: [
      'januari', 'februari', 'mars',
      'april', 'maj', 'juni', 'juli',
      'augusti', 'september', 'oktober',
      'november', 'december'
    ]
  },
  page: {
    page: {
      policy: 'Tillbaka till integritetspolicyn',
      top: 'Tillbaka till toppen',
      post: {

      }
    },
    inspiration: {
      latest: 'Senaste artiklarna',
      readmore: 'Visa alla',
      category: {
        selectAll: 'Alla',

        post: {
          back: 'Tillbaka',
          about: 'Om oss',
          other: 'Andra artiklar för dig',
          backLink: {
            article: '/inspiration',
            job: '/jobba-hos-oss',
            about: '/om-oss'
          }
        }
      }
    }
  },
  component: {
    the: {
      header: {
        menu: 'MENY',
        search: 'Sök'
      },
      footer: {
        cookies: {
          content: 'K-Fastigheter.com använder cookies för att förbättra webbplatsen utifrån webbstatistik. All statistik förs anonymt hos vår leverantör. Om du inte godkänner cookies får vi sämre underlag för att förbättra vår hemsida. Om du blockerar cookies i din webbläsare visas detta meddelande igen.',
          url: '/cookiepolicy',
          btnText: 'Du hittar detaljerad information här.'
        }
      }
    },
    base: {
      archiveFilter: {
        sort: 'Sortera efter',
        placeholder: 'Sök bland våra artiklar...',
        order: 'Senaste',
        category: 'Alla',
        searchPage: '/inspiration/alla'
      },
      financalReports: {
        title: 'Finansiella rapporter',
        url: '/for-investerare/finansiella-rapporter/',
        docs: 'Dokument från'
      },
      pressRelease: {
        title: 'Pressmeddelanden',
        path: '/for-investerare/press/',
        pdf: 'Ladda ner PDF'
      },
      cta: {
        readmore: 'Läs mer',
        url: '/bolagsstyrning'
      },
      postCard: {},
      postGate: {
        btnText: 'Nyfiken på mer?'
      },
      postList: {},
      postSlider: {
        readmore: 'Visa alla'
      },
      postTall: {},
      postWide: {
        readmore: 'Läs mer'
      }
    },
    ir: {
      back: 'Tillbaka',
      // link
      cisionArchiveCard: {
        url: '/for-investerare/press/',
        pdf: 'Ladda ner PDF'
      },
      contentTypeCisionArchive: {
        type: {
          options: [
            'Alla pressmeddelanden',
            'Regulatoriska pressmeddelanden',
            'Övriga pressmeddelanden'
          ]
        },
        year: {
          options: [
            'År'
          ]
        }
      },
      contentTypeCisionContent: {
        docks: 'Dokument'
      },
      contentTypeCisionStocks: {
        tab: ['Aktiedata', 'Orderbok'],
        Symbol: 'Symbol',
        TradeCurrency: 'Valuta',
        MarketPlace: 'Marknadsplats',
        ClosePrice1D: 'Stängningspris',
        LastPrice: 'Senaste pris',
        HighPriceYtd: 'Årshögsta',
        LowPriceYtd: 'Årslägsta',
        Ath: 'Toppnotering',
        Atl: 'Lägsta notering',
        NumberOfShares: 'Antal aktier',
        Buy: 'Köp',
        Volume: 'Volym',
        Sell: 'Sälj'
      },
      contentTypeCisionSpecial: {
        press: {
          title: 'Senaste pressmeddelandet',
          url: '/for-investerare/press',
          readmore: 'Läs alla pressmeddelanden'
        },
        reports: {
          title: 'Delårsrapporter',
          url: '/for-investerare/finansiella-rapporter',
          readmore: 'Läs alla delårsrapporter'
        },
        calendar: {
          title: 'Kommande händelser',
          url: '/for-investerare/finansiell-kalender',
          readmore: 'Läs alla händelser'
        }
      },
      contentTypeWordpressContent: {
        docks: 'Dokument från',
        files: {
          finansiellaRapporter: {
            all: 'Finansiella rapporter från alla år',
            option: 'Finansiella rapporter från'
          },
          bolagsstamma: {
            all: 'Samtliga Bolagsstämmor',
            option: 'Bolagsstämma '
          }

        }
      },
      contentTypeWordpressContentFilter: {
        livein: {
          label: 'Vänligen välj det land du är bosatt i',
          placeholder: 'Land'
        },
        isin: {
          label: 'Vänligen välj det land du befinner dig i',
          placeholder: 'Land'
        },
        confirm: 'Jag bekräftar',
        deny: 'Jag bekräftar inte'
      },
      navbar: {},
      sidebar: {
        navTitle: 'Navigering',
        contactTitle: 'Kontakt',
        subscribeTitle: 'Prenumerera'
      },
      sidebarForm: {
        title: 'Förbli uppdaterad, prenumerera på vår information',
        subTitle: 'Vad är du intresserad av?',
        gdpr: 'När du prenumererar på våra utskick så kommer K-Fast Holding AB, 556827-0390, att behandla de personuppgifter om dig som du lämnar i formuläret ovan samt eventuella ändringar i din prenumeration som du gör via den länk som vi kommer att skicka till dig. Vi kommer att lagra dessa uppgifter samt använda dem när vi gör dina prenumerationsutskick och när du går in på inställningarna för din prenumeration. Ändamålet med behandlingen är att tillhandahålla dig den information som du önskar prenumerera på samt att administrera din prenumeration. Din prenumeration tillhandahålls med hjälp av vårt personuppgiftsbiträde Cision, som också kommer skicka ut en länk till dig för att aktivera prenumerationen (vilket du måste göra inom 25 dagar, annars tas den bort). <br/><br/> Nu undrar vi om du vill ge ditt samtycke till denna personuppgiftsbehandling. Du kan när som återkalla ditt samtycke med verkan för framtiden, genom att använda länken som du hittar i det mail som du får när du har bekräftat din prenumeration, liksom i alla våra utskick, eller genom att kontakta oss. <a href="/integritetspolicy">Här</a> hittar du mer information om hur K-Fast hanterar dina personuppgifter.',
        gdprLabel: 'OK – Jag samtycker',
        send: 'Skicka',
        sent: 'Skickat!',
        res: {
          good: 'Skickat! Invänta bekräftelse E-mail.',
          bad: 'Något gick fel. Kontrollera att du har skrivit en giltig E-mail.'
        },
        form: {
          types: [
            'Årsredovisning',
            'Rapporter',
            'Pressmeddelanden'
          ],
          name: {
            placeholder: 'Skriv namn här...',
            label: 'Namn'
          },
          phone: {
            placeholder: 'Skriv telefon här...',
            label: 'Telefon'
          },
          email: {
            placeholder: 'Skriv e-post här...',
            label: 'E-post'
          },
          company: {
            placeholder: 'Skriv företag här...',
            label: 'Företag'
          },
          lang: {
            placeholder: 'Välj språk...',
            label: 'Språk',
            options: ['Svenska', 'English'],
            default: 'sv'
          }
        }
      },
      wordpressCalendarList: {
        title: 'Kommande händelser',
        order: 'Händelser från'
      },
      wordpressPepoleList: {}
    }
  }

}

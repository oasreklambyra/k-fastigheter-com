import Vue from 'vue'

Vue.mixin({
  data() {
    return {
      vueWindow: {
        scroll: 0,
        width: 0
      }
    }
  },
  mounted() {
    this.vueWindow.width = window.innerWidth

    window.addEventListener('resize', () => {
      this.vueWindow.width = window.innerWidth
    })

    window.addEventListener('scroll', () => {
      this.vueWindow.scroll = window.pageYOffset
    })
  },
  methods: {
    isGoogleTranslated() {
      return this.$route.name === 'ledigt-post___en' ||
        this.$route.name === 'fastighet-post___en' ||
        this.$route.name === 'inspiration-category-post___en' ||
        this.$route.name === 'felanmalan-faq___en'
    },
    isIE() {
      if (process.client && navigator.userAgent.match(/Trident\/7\./)) {
        return true
      } else {
        return false
      }
    },

    sameLang(url) {
      const lang = this.$store.state.i18n.locale

      const containsEn = url.includes('/en/')

      if (lang === 'en' && containsEn) {
        return true
      }

      if (lang === 'sv' && !containsEn) {
        return true
      }

      return false
    },

    getImageSize(maxSize) {
      const imageSizes = {
        mobile: 0,
        tablet: 576,
        laptop: 768,
        desktop: 992,
        max: 1080
      }

      let res = 'mobile'

      if (process.browser) {
        Object.keys(imageSizes).forEach((key) => {
          if (imageSizes[maxSize] >= imageSizes[key] && window.innerWidth >= imageSizes[key]) {
            res = key
          }
        })
      }

      return res
    },

    formatDate(string) {
      if (string) {
        const d = string.split(/[- :T]/)
        const date = new Date(d[0], d[1] - 1, d[2])
        const monthNames = this.$t('global.months')

        const day = date.getDate()
        const monthIndex = date.getMonth()
        const year = date.getFullYear()

        return day + ' ' + monthNames[monthIndex] + ' ' + year
      } else {
        return ''
      }
    },

    formatUrl(post) {
      if (post.post_name === 'att-bo-hos-oss' || post.post_name === 'miljo-kvalitet' || post.post_name === 'projektutveckling' || post.post_name === 'byggnation' || post.post_name === 'forvaltning') {
        return '/om-oss/' + post.post_name
      }

      if (post.post_name === 'being-our-tenant' || post.post_name === 'environment-quality' || post.post_name === 'projectdevelopment' || post.post_name === 'construction' || post.post_name === 'management') {
        return '/en/about-us/' + post.post_name
      }

      if (post.post_type === 'job') {
        let path = '/jobb/'

        if (this.$store.state.i18n.locale === 'en') {
          path = '/en/job/'
        }

        return path + post.post_name
      }

      if (post.post_type === 'article') {
        let path = '/inspiration/' + post.post_taxs.category[0].slug + '/' + post.post_name

        if (this.$store.state.i18n.locale === 'en') {
          path = '/en/inspiration/' + post.post_taxs.category[0].slug + '/' + post.post_name
        }

        return path
      }

      if (post.post_type === 'apartment') {
        let path = '/ledigt/'

        if (this.$store.state.i18n.locale === 'en') {
          path = '/en/available/'
        }

        return path + post.post_name
      }

      if (post.post_type === 'real-estate') {
        let path = '/fastighet/'

        if (this.$store.state.i18n.locale === 'en') {
          path = '/en/real-estate/'
        }

        return path + post.post_name
      }

      if (post.post_type === 'page') {
        return (post.permalink) ? post.permalink : post.post_name
      }

      return '/' + post.post_type + '/' + post.post_name
    },

    urltoTemplate(url) {
      url = url.slice(1)

      if (url === '') {
        url = 'index'
      }

      return url
    },

    isMobile() {
      if (process.client) {
        return window.innerWidth < 576
      } else {
        return false
      }
    },

    isTablet() {
      if (process.client) {
        return window.innerWidth < 991
      } else {
        return false
      }
    },

    scrolled() {
      if (process.client) {
        return this.vueWindow.scroll || 0
      } else {
        return 0
      }
    }

  }
})

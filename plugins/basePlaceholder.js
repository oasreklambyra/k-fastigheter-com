import Vue from 'vue'
import BasePlaceholder from '../components/BasePlaceholder.vue'

Vue.component('BasePlaceholder', BasePlaceholder)

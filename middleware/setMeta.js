export default function (context) {
  // const i18nSeo = this.$nuxtI18nSeo()
  let postType = 'any'

  // set path

  let path = context.route.path

  path = (path.endsWith('/')) ? path.slice(0, -1) : path

  const pathArr = path.split('/')
  path = pathArr[pathArr.length - 1]

  if (path === '' || path === 'en') {
    path = 'startsida'
  }

  // router logic
  const route = context.route

  if (route.name) {
    if (route.name.includes('ledigt')) {
      postType = 'apartment'
    } else if (route.name.includes('fastighet') && route.params.post) {
      postType = 'real-estate'
    } else if (route.name.includes('hyresfastigheter') && route.params.type) {
      path = 'hyresfastigheter'
      postType = 'page'
    } else if (route.name.includes('jobb') && route.params.job) {
      postType = 'job'
    } else if (route.name.includes('inspiration')) {
      if (route.params.post) {
        postType = 'article'
      }
      if (!route.params.post) {
        path = 'inspiration'
        postType = 'page'
      }
    } else {
      postType = 'page'
    }
  }

  let meta = context.app.store.getters.getPage[postType + '/' + path]
  if (meta) {
    meta = meta.yoast_meta
  }

  const metaCision = context.app.store.getters['cision/getCisionSingle'][path]

  if (meta) {
    const metaTags = [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: meta.yoast_wpseo_metadesc },
      { hid: 'twitter:card', name: 'twitter:card', content: 'summary' },
      { hid: 'twitter:description', name: 'twitter:description', content: meta['yoast_wpseo_twitter-description'] },
      { hid: 'twitter:title', name: 'twitter:title', content: meta['yoast_wpseo_twitter-title'] },
      { hid: 'twitter:image', name: 'twitter:image', content: meta['yoast_wpseo_twitter-image'] },
      { hid: 'og:locale', property: 'og:locale', content: context.app.store.state.i18n.locale },
      { hid: 'og:type', property: 'og:type', content: 'website' },
      { hid: 'og:title', property: 'og:title', content: meta['yoast_wpseo_opengraph-title'] },
      { hid: 'og:description', property: 'og:description', content: meta['yoast_wpseo_opengraph-description'] },
      { hid: 'og:url', property: 'og:url', content: 'https://k-fastigheter.com' + context.route.path },
      { hid: 'og:site_name', property: 'og:site_name', content: meta.yoast_wpseo_title },
      { hid: 'og:image', property: 'og:image', content: meta['yoast_wpseo_opengraph-image'] }
      // ...i18nSeo.meta
    ]

    const linksTags = [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.jpg' },
      { hid: 'canonical', rel: 'canonical', href: 'https://k-fastigheter.com' + context.route.path }
      // ...i18nSeo.link
    ]

    const htmlAttrsTags = {
      // ...i18nSeo.htmlAttrs
    }

    context.app.head.title = meta.yoast_wpseo_title.replace(/<[^>]*>?/gm, '')
    context.app.head.meta = metaTags
    context.app.head.link = linksTags
    context.app.head.htmlAttrs = htmlAttrsTags
  } else if (metaCision) {
    let image = 'https://wordpress.k-fastigheter.com/wp-content/uploads/2021/02/DSC5320-scaled.jpg'

    if (metaCision.Release.Images.length > 0) {
      image = metaCision.Release.Images[0].UrlTo800x800ArResized
    }
    const metaTags = [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: metaCision.Release.Intro },
      { hid: 'twitter:card', name: 'twitter:card', content: 'summary' },
      { hid: 'twitter:description', name: 'twitter:description', content: metaCision.Release.Intro },
      { hid: 'twitter:title', name: 'twitter:title', content: metaCision.Release.Title },
      { hid: 'twitter:image', name: 'twitter:image', content: image },
      { hid: 'og:locale', property: 'og:locale', content: context.app.store.state.i18n.locale },
      { hid: 'og:type', property: 'og:type', content: 'website' },
      { hid: 'og:title', property: 'og:title', content: metaCision.Release.Title },
      { hid: 'og:description', property: 'og:description', content: metaCision.Release.Intro },
      { hid: 'og:url', property: 'og:url', content: 'https://k-fastigheter.com' + context.route.path },
      { hid: 'og:site_name', property: 'og:site_name', content: metaCision.Release.Title },
      { hid: 'og:image', property: 'og:image', content: image }

      // ...i18nSeo.meta
    ]

    const linksTags = [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.jpg' },
      { hid: 'canonical', rel: 'canonical', href: 'https://k-fastigheter.com' + context.route.path }
      // ...i18nSeo.link
    ]

    const htmlAttrsTags = {
      // ...i18nSeo.htmlAttrs
    }

    context.app.head.title = metaCision.Release.Title
    context.app.head.meta = metaTags
    context.app.head.link = linksTags
    context.app.head.htmlAttrs = htmlAttrsTags
  } else {
    // context.router.push('/')

    context.app.head.title = 'Loading K-Fastigheter'
  }
}

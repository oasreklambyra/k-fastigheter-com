export default async function (context) {
  let postType = 'any'
  // set path

  let path = context.route.path

  path = (path.endsWith('/')) ? path.slice(0, -1) : path

  const pathArr = path.split('/')
  path = pathArr[pathArr.length - 1]

  if (path === '' || path === 'en') {
    path = 'startsida'
  }

  const route = context.route

  let lang = 'sv'

  if (context.route.name.includes('_en')) {
    lang = 'en'
  }

  if (route.name) {
    if (route.name.includes('ledigt')) {
      postType = 'apartment'
    } else if (route.name.includes('fastighet') && route.params.post) {
      postType = 'real-estate'
    } else if (route.name.includes('hyresfastigheter') && route.params.type) {
      path = 'hyresfastigheter'
      postType = 'page'
    } else if (route.name.includes('jobb') && route.params.job) {
      postType = 'job'
    } else if (route.name.includes('inspiration')) {
      if (route.params.post) {
        postType = 'article'
      }

      if (route.params.category && !route.params.post) {
        path = 'inspiration'
        postType = 'page'
      }
    } else {
      postType = 'page'
    }

    // Set posttype
    if (context.route.name.includes('for-investerare-press-page')) {
      if (!context.app.store.getters['cision/getCisionSingle'][path]) {
        await context.app.store.dispatch({ type: 'cision/getCisionSingle', id: path, detailLevel: 'medium', lang: lang })
      }
    } else if (!context.app.store.getters.getPage[postType + '/' + path]) {
      await context.app.store.dispatch({ type: 'getPage', path: path, postType: postType, lang: lang })
    }
  }

  if (context.route.name.includes('aktie')) {
    await context.app.store.dispatch({ type: 'cision/getCisionShare', lang: lang })
    await context.app.store.dispatch({ type: 'cision/getCisionPerformance', lang: lang })
    await context.app.store.dispatch({ type: 'cision/getCisionOrderbook', lang: lang })
  }
}

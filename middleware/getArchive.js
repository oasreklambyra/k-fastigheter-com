export default async function (context) {
  // set path

  let path = context.route.path

  path = (path.endsWith('/')) ? path.slice(0, -1) : path

  const pathArr = path.split('/')
  path = pathArr[pathArr.length - 1]

  let lang = 'sv'

  if (context.route.name.includes('_en')) {
    lang = 'en'
  }

  const apartment = ['hyresfastigheter', 'parkeringar', 'residential-properties', 'parking']

  if (apartment.includes(path)) {
    if (!context.app.store.getters.getCpt.apartment) {
      await context.app.store.dispatch({ type: 'getCpt', postType: 'apartment', lang: lang })
    }
  }

  const estate = ['under-produktion', 'under-forvaltning', 'kommersiella', 'managed-properties', 'properties-under-development', 'commercial-properties']

  if (estate.includes(path)) {
    if (!context.app.store.getters.getCpt['real-estate']) {
      await context.app.store.dispatch({ type: 'getCpt', postType: 'real-estate', lang: lang })
    }
  }

  if (path === 'inspiration') {
    if (!context.app.store.getters.getCpt.article) {
      await context.app.store.dispatch({ type: 'getCpt', postType: 'article', lang: lang })
    }
  }

  if (!context.app.store.getters.getOptions) {
    await context.app.store.dispatch({ type: 'getOptions', lang: lang })
  }

  if (!context.app.store.getters.getSitemap) {
    await context.app.store.dispatch({ type: 'getSitemap', lang: lang })
  }
}

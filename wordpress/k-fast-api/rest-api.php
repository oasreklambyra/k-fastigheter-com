<?php

/**************/
/*   Routes   */
/**************/

add_action( 'rest_api_init', 'extend_rest_routes' );

function extend_rest_routes() {

	/*
		Todo:
		- Move all requests to core, keep old
		- Update Nuxt to work with new
		- Remove old
	 */

	//Is used
	register_rest_route( 'main',
		'page/slug=(?P<slug>[a-zA-Z0-9-]+)&type=(?P<type>[a-zA-Z0-9-]+)&lang=(?P<lang>[a-zA-Z0-9-]+)',
		array(
        	array(
	            'callback' => 'api_get_page'
        	)
    	)
    );


    //Not used 
	register_rest_route( 'preview',
		'page/slug=(?P<slug>[a-zA-Z0-9-]+)&type=(?P<type>[a-zA-Z0-9-]+)&lang=(?P<lang>[a-zA-Z0-9-]+)',
		array(
        	array(
	            'callback' => 'api_get_page_preview'
        	)
    	)
    );

	// Not used 
    register_rest_route( 'meta',
		'page/slug=(?P<slug>[a-zA-Z0-9-]+)&type=(?P<type>[a-zA-Z0-9-_]+)&lang=(?P<lang>[a-zA-Z0-9-]+)',
		array(
        	array(
	            'callback' => 'api_get_page_meta'
        	)
    	)
    );

    //Is used
	register_rest_route( 'preview',
		'cpt/type=(?P<type>[a-zA-Z0-9-]+)&lang=(?P<lang>[a-zA-Z0-9-]+)',
		array(
        	array(
	            'callback' => 'api_get_cpt'
        	)
    	)
    );

	//Is used
    register_rest_route( 'preview',
		'taxonomy/type=(?P<type>[a-zA-Z0-9-_]+)&lang=(?P<lang>[a-zA-Z0-9-]+)',
		array(
        	array(
	            'callback' => 'api_get_taxonomy'
        	)
    	)
    );

	//Is used
    register_rest_route( 'main',
		'options/lang=(?P<lang>[a-zA-Z0-9-]+)',
		array(
        	array(
	            'callback' => 'api_get_options'
        	)
    	)
    );

    //Is used 
    register_rest_route( 'main',
		'sitemap/lang=(?P<lang>[a-zA-Z0-9-]+)',
		array(
        	array(
	            'callback' => 'api_get_sitemap'
        	)
    	)
    );

    //Is used
    register_rest_route( 'main',
		'form',
		array(
			'methods' => 'POST',
	        'callback' => 'api_post_form'
    	)
    );


    /* NEW */

    register_rest_route( 'core', 'page', array(
    	array(
            'callback' => 'api_get_page',
            'args' => array(
            	'slug',
            	'type'
            )
    	)
    	
    )); 

    //Adds route /core/cpt/type=[post_type]
	register_rest_route( 'core', 'cpt', array(
    	array(
            'callback' => 'api_get_cpt',
            'args' => array(
               	'type'
            )
    	)
	));

	//Adds route /core/taxonomy/type=[tax_slug]
    register_rest_route( 'core', 'taxonomy', array(
    	array(
            'callback' => 'api_get_taxonomy',
            'args' => array(
               	'type'
            )
    	)
    ));

	//Adds route /core/options
   register_rest_route( 'core', 'options', array(
    	array(
            'callback' => 'api_get_options'
    	)
	));

    //Adds route /core/sitemap
    register_rest_route( 'core', 'sitemap', array(
    	array(
            'callback' => 'api_get_sitemap'
    	)
	));
}

/**********************/
/*   Route callbacks   */
/**********************/

function set_content_type( $content_type ) {
	return 'text/html';
}

function api_post_form($data) {
	$parameters = $data->get_params()['body'];

	$to = '';
	$subject = '';
	$header = '';

	if($parameters['formType'] == 'interest') {
		$city = (strip_tags($parameters['otherCity']) !== '')? ' i ' . strip_tags($parameters['otherCity']) : '';
		$area = (strip_tags($parameters['otherArea']) !== '')? ', ' . strip_tags($parameters['otherArea']) : '';

		$header = 'Intresseanmälan i staden: ' . $city . $area ;

		if($parameters['object']) {
			$header = 'Intresseanmälan: ' . $parameters['object'];
		}

		$to = 'intresseanmalan@k-fastigheter.se';	
		$subject = $header;
	}

	if($parameters['formType'] == 'application') {
		$city = (strip_tags($parameters['otherCity']) !== '')? ' i ' . strip_tags($parameters['otherCity']) : '';
		$area = (strip_tags($parameters['otherArea']) !== '')? ', ' . strip_tags($parameters['otherArea']) : '';

		$header = 'Ansökan: ospecifierat object i' . $city . $area ;

		if($parameters['object']) {
			$header = 'Ansökan: ' . $parameters['object'];
		}

		$to = 'intresseanmalan@k-fastigheter.se';	
		$subject = $header;
	}

	if($parameters['formType'] == 'contact') {
		$header = 'Kontaktformulär';

		$to = 'info@k-fastigheter.se';	
		$subject = $header;
	}

	if($parameters['formSize'] == 'contact') {
		$req = array('nameLast', 'email', 'other');
		

		$format_params = array(
			'Namn' => strip_tags($parameters['nameFirst']) . ' ' . strip_tags($parameters['nameLast']), 
			'Telefon' => strip_tags($parameters['phone']),
			'E-post'  => strip_tags($parameters['email']),
			'Ämne' => strip_tags($parameters['subject']),
			'Meddelande' => strip_tags($parameters['other'])
		);
	
	}


	/*if($parameters['formSize'] == 'small') {
		$req = array('nameFirst', 'nameLast', 'phone', 'email');
		

		$format_params = array(
			'Namn' => strip_tags($parameters['nameFirst']) . ' ' . strip_tags($parameters['nameLast']), 
			'Telefon' => strip_tags($parameters['phone']),
			'E-post'  => strip_tags($parameters['email']),
			'Stad' => strip_tags($parameters['cityArea']),
			'Rum' => strip_tags($parameters['rooms']),
			'Övrigt' => strip_tags($parameters['other'])
		);
	
	}*/


	if($parameters['formSize'] == 'big') {
		$req = array('nameFirst', 'nameLast', 'ssn', 'email', 'phone', 'adressName', 'adressArea', 'adressNumber', 'work', 'income', 'movingDate');

		$format_params = array(
			'Namn' => strip_tags($parameters['nameFirst']) . ' ' . strip_tags($parameters['nameLast']), 
			'Telefon' => strip_tags($parameters['phone']),
			'Personnummer' => strip_tags($parameters['ssn']),
			'E-post'  => strip_tags($parameters['email']),
			'Telefon'  => strip_tags($parameters['phone']),
			'Adress'  => strip_tags($parameters['adressName']) . ' ' . strip_tags($parameters['adressNumber']) . ', ' . strip_tags($parameters['adressArea']),
			'Sysselsättning'  => strip_tags($parameters['work']),
			'Årsinkomst'  => strip_tags($parameters['income']),

			'Medsökande namn' => ($parameters['coNameFirst'] !== '')? strip_tags($parameters['coNameFirst']) . ' ' . strip_tags($parameters['coNameLast']): '', 
			'Medsökande personnummer' => strip_tags($parameters['coSsn']),
			'Medsökande e-post'  => strip_tags($parameters['coEmail']),
			'Medsökande sysselsättning'  => strip_tags($parameters['coWork']),
			'Medsökande årsinkomst'  => strip_tags($parameters['coIncome']),

			'Antal boende'  => strip_tags($parameters['pepoleInApartment']),
			'Önskad våning' => strip_tags($parameters['floor']),
			'Tillgänglig för flytt' => strip_tags($parameters['movingDate']),
			'Antal rum' => strip_tags($parameters['rooms']),

			'Husdjur' => (strip_tags($parameters['pets']))? 'Ja' : 'Nej',
			'Rökning' => (strip_tags($parameters['smoking']))? 'Ja' : 'Nej',

			'Önskar även att söka i' => strip_tags($parameters['otherCity']) . ' ' . strip_tags($parameters['otherArea']),
			'Övrigt' => strip_tags($parameters['otherOther'])
		);
	
	}

	
	foreach ($parameters as $key => $value) {
		if(in_array($key, $req) && !$value) {
			return new WP_REST_Response( array('message' => 'Missing parameter(s)', 'error' => true) , 200 );
		}

		if(preg_match('/script|<|>|src|.js/', $value)) {
			return new WP_REST_Response( array('message' => 'Unallowed characters(s)', 'error' => true) , 200 );
		}
	}
	

	$message = '<html><body>';
	$message .= '<h1>' . $header .  '</h1>';

	if($parameters['formSize'] !== 'contact') {
		$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';

		foreach ($format_params as $key => $value) {
			if($value !== '') {
				$message .= '<tr><td><strong>' . $key . '</strong> </td><td>' . $value . '</td></tr>';
			}
			
		}
		
		$message .= "</table>";	
	}

	if($parameters['formSize'] == 'contact') {
		foreach ($format_params as $key => $value) {
			$message .= '<div>' . $key . ': ' . $value . '</div>';
		}
	}

	$message .= "</body></html>";

	$headers = array();
    $attachments = array();


    //$to = 'pierre.norrbrink@oas.nu';
    
    add_filter( 'wp_mail_content_type', 'set_content_type' );
    $email = wp_mail($to, $subject, $message, $headers, $attachments);

	wp_mail($parameters['email'], 'Email received', 'Thank you for your E-mail. This is a verification what we have recived your E-mail and will be in contact with you soon.');    
    remove_filter( 'wp_mail_content_type', 'set_content_type' );
	// Response setup
	//return new WP_REST_Response( $email , 200 );
	return new WP_REST_Response( $message , 200 );
}


function api_get_options($data) {

	$options = 'options';

	if($data['lang'] == 'en') {
		$options = 'options_en';
	}
 
	$routes = get_fields($options);

	return new WP_REST_Response( $routes, 200 );	
}

function api_get_sitemap($data) {

	global $sitepress;
	$sitepress->switch_lang($data['lang']);

	$args = array(
		'post_type'              => array('apartment', 'page', 'article', 'real-estate'),
		'nopaging'               => true,
		'posts_per_page'         => '-1',
		'post_status' 			 => 'publish'
	);

	// The Query
	$query = new WP_Query( $args );

	$routes = [];

	foreach ($query->posts as $post) {
		$objects = array(
			'post_date_gmt',
			'post_author',
			'post_parent',
			'menu_order',
			'post_status',
			'comment_status',
			'ping_status',
			'post_password',
			'to_ping',
			'pinged',
			'post_modified',
			'post_modified_gmt',
			'post_content_filtered',
			'guid',
			'post_mime_type',
			'comment_count',
			'filter',
			'post_content',
			'post_excerpt',
			'post_date'
		);

		foreach ($objects as $object) {
			unset($post->{$object});
		}

		if($post->post_type === 'article' || $post->post_type === 'apartment' || $post->post_type === 'real-estate') {
			$post_taxs = get_object_taxonomies($post->post_type);
			$post->post_taxs = array();
			
			foreach ($post_taxs as $post_tax) {
				$post->post_taxs[$post_tax] = get_the_terms($post->ID, $post_tax);
			}
		}

		$post->permalink = str_replace(home_url(), '', get_permalink($post->ID));

		if($post->ID !== 1488) {
			$routes[] = $post;	
		}
		
	}

	// Response setup
	return new WP_REST_Response( $routes, 200 );
}



function api_get_page($data) {

	global $sitepress;
	$sitepress->switch_lang($data['lang']);


	$args = array(
		'post_type'              => $data['type'],
		'nopaging'               => true,
		'posts_per_page'         => '-1',
		'name'					 => $data['slug'],
	);

	// The Query
	$query = new WP_Query( $args );

	$routes = [];

	foreach ($query->posts as $post) {
		$routes[] = add_custom_tags_to_response($post);
	}

	// Response setup
	return new WP_REST_Response( $routes, 200 );
}

function api_get_page_preview($data) {

	global $sitepress;
	$sitepress->switch_lang($data['lang']);
	
	$args = array(
		'post_type'              => explode(',',$data['type']),
		'nopaging'               => true,
		'posts_per_page'         => '-1',
		'name'					 => $data['slug'],
	);

	$query = new WP_Query( $args );

	$routes = [];

	foreach ($query->posts as $key => $post) {

		//Images
		$post->featured_image = array('full' => get_the_post_thumbnail($post->ID, 'full'));
		$routes[] = $post;
	}

	
	// Response setup
	return new WP_REST_Response( $routes, 200 );
}


function api_get_page_meta($data) {

	global $sitepress;
	$sitepress->switch_lang($data['lang']);
	
	$args = array(
		'post_type'              => array( $data['type'] ),
		'nopaging'               => true,
		'posts_per_page'         => '-1',
		'name'					 => $data['slug'],
	);

	// The Query
	$query = new WP_Query( $args );

	$routes = [];

	foreach ($query->posts as $post) {
		$post = json_decode(json_encode($post), true);


		$post['yoast_meta']  = wp_api_encode_yoast($post['ID']);
		$routes[] = $post['yoast_meta'];
	}

	if(count($query->posts) == 0) {
		$terms = get_terms( $data['type']);

		foreach ($terms as $key_term => $term) {
			if($term->slug == $data['slug']) {
				$term->yoast_meta = wp_api_encode_yoast($term->term_id);
				$routes[] = $term['yoast_meta'];
			}
		}	
	}

	// Response setup
	return new WP_REST_Response( $routes, 200 );
}

function api_get_cpt($data) {


	global $sitepress;
	$sitepress->switch_lang($data['lang']);


	$args = array(
		'post_type'              => $data['type'],
		'nopaging'               => true,
		'posts_per_page'         => '-1',
		'orderby'				 => 'title',
		'post_status'			 => array('publish')
	);

	// The Query
	$query = new WP_Query( $args );

	$routes = [];

	$posts = json_decode(json_encode($query->posts), true);


	foreach ($posts as $post) {
		$objects = array(
			'post_date_gmt',
			'post_author',
			'post_parent',
			'menu_order',
			'post_status',
			'comment_status',
			'ping_status',
			'post_password',
			'to_ping',
			'pinged',
			'post_modified',
			'post_modified_gmt',
			'post_content_filtered',
			'guid',
			'post_mime_type',
			'comment_count',
			'filter'
		);

		if($data['type'] !== 'faq') {
			$objects[] = 'post_content';
		}

		foreach ($objects as $object) {
			unset($post{$object});
		}

		if($data['type'] !== 'faq') {
			$post['featured_image'] = get_image_src(get_post_thumbnail_id($post['ID']));
			$post['custom_fields'] = get_acf_fields($post['ID']);			
		}




		$post_taxs = get_object_taxonomies($post['post_type']);

		foreach ($post_taxs as $post_tax) {
			$post['post_taxs'][$post_tax] = get_the_terms($post['ID'], $post_tax);

			if($post['post_taxs'][$post_tax]) {
				foreach ($post['post_taxs'][$post_tax] as $key => $tax_item) {
					$post['post_taxs'][$post_tax][$key]->verb = get_field('verb', $tax_item->taxonomy . '_' . $tax_item->term_id);
				}	
			}
			
		}

		$routes[] = $post;
	}

	


	// Response setup
	return new WP_REST_Response( $routes, 200 );
}

function api_get_taxonomy($data) {


	global $sitepress;
	$sitepress->switch_lang($data['lang']);
	
	$tax = get_terms( array(
	    'taxonomy' => $data['type'],
	    'hide_empty' => false,
	) );

	// Response setup
	return new WP_REST_Response( $tax, 200 );
}

/***************************************************************/
/*   add_custom_tags_to_response adds logic to post and pages   */
/***************************************************************/

function add_custom_tags_to_response( $post ) {
	global $shortcode_tags;


	$post = json_decode(json_encode($post), true);

	$page_template = get_field('page_template', $post['ID']);

	//content formating
	remove_filter( 'the_content', 'do_shortcode', 11 );
	$post['post_content'] = apply_filters('the_content', $post['post_content']);
	add_filter( 'the_content', 'do_shortcode', 11 );

	//trigger shortcodes
	$post_content = $post['post_content'];

	preg_match_all( '@\[([^<>&/\[\]\x00-\x20=]++)@', $post_content, $matches );
    $tagnames = array_intersect( array_keys( $shortcode_tags ), $matches[1] );
    $m = array();

    //Images
	$post['featured_image'] = get_image_src(get_post_thumbnail_id($post['ID']));

	//Tags
	$post_taxs = get_object_taxonomies($post['post_type']);

	foreach ($post_taxs as $post_tax) {
		$post['post_taxs'][$post_tax] = get_the_terms($post['ID'], $post_tax);
	}	

    //Yoast
	$post['yoast_meta'] = wp_api_encode_yoast($post['ID']);

	//Custom fields
	$post['custom_fields'] = get_acf_fields($post['ID']);

	//WPML
	//$post['language'] = apply_filters( 'wpml_post_language_details', NULL, $post['ID'] );

	//Remove some objects
	$objects = array(
		'post_date_gmt',
		'post_status',
		'comment_status',
		'ping_status',
		'post_password',
		'to_ping',
		'pinged',
		'post_modified',
		'post_modified_gmt',
		'post_content_filtered',
		'guid',
		'post_mime_type',
		'comment_count',
		'filter'
	);

	foreach ($objects as $object) {
		unset($post[$object]);
	}

	return $post;
}


function get_acf_fields($post_ID) {
	$fields = get_fields($post_ID);

	return $fields;
}

function add_images( $value, $post_id, $field ) {
	$value = get_image_src($value);

    return $value;
}


//add_filter('acf/load_value/name=background_image', 'add_images', 10, 3);
add_filter('acf/format_value/type=image', 'add_images', 15, 3);


/*
function add_post_data( $value, $post_id, $field ) {
	$res = array();

	if(is_array($value)){
		foreach ($value as $key => $v) {
			if(is_object($v)) {
				$res[] = add_custom_tags_to_response($v);	
			} else {
				$res[] = $v;
			}
		}
	} else {
		$res = $value;
	}

	//$value = add_custom_tags_to_response($value);

    return $res;
}

add_filter('acf/format_value/type=relationship', 'add_post_data', 15, 3);
*/

//Add ACF to relationship 
add_filter('acf/format_value/type=relationship', 'add_post_data', 15, 3);
add_filter('acf/format_value/type=post_object', 'add_post_data', 15, 3);
function add_post_data( $value, $post_id, $field ) {
	$res = array();

	if(is_array($value)){
		foreach ($value as $key => $v) {

			if(is_object($v)) {

				$res[] = add_custom_tags_to_response($v);	
			} else {
				$res[] = $v;
			}
		}
	} elseif (is_object($value)) {
		

		//$res = add_custom_tags_to_response($value);
		$value->featured_image = get_image_src(get_post_thumbnail_id($value->ID));	
		$value->permalink = str_replace(home_url(), '', get_permalink($value->ID));

		$res = $value;	
		
	} else {
		$res = $value;
	}


    return $res;
}



function get_image_src( $id ) {
	// Grab all available image sizes which have been declared with 'add_image_size'
	$image_sizes = get_intermediate_image_sizes();

	$image_sizes[] = 'full';

	foreach($image_sizes as $image_size) {
		$feat_img_array[$image_size] = wp_get_attachment_image_src( $id, $image_size, false)[0];
	}

	$image = is_array( $feat_img_array ) ? $feat_img_array : 'false';

	return $image;
}

function wp_api_encode_yoast($post_ID, $term = false) {

	$yoastMeta = '';
	if ( class_exists( 'WPSEO_Frontend_To_REST_API' ) ) {
		$wpseo_frontend = WPSEO_Frontend_To_REST_API::get_instance();
		$wpseo_frontend->reset();


		query_posts( array(
			'p'         => $post_ID, // ID of a page, post, or custom type
			'post_type' => 'any'
		) );
		the_post();


	    $yoastMeta = array(
	        'yoast_wpseo_focuskw' => get_post_meta($post_ID, '_yoast_wpseo_focuskw', true),
	       	'yoast_wpseo_title'     => $wpseo_frontend->get_content_title(),
	        'yoast_wpseo_metadesc'  => $wpseo_frontend->metadesc( false ),
	        'yoast_wpseo_linkdex' => get_post_meta($post_ID, '_yoast_wpseo_linkdex', true),
	        'yoast_wpseo_metakeywords' => $wpseo_frontend->metakeywords( ),
	        'yoast_wpseo_canonical' => str_replace('startsida/', '', str_replace('wordpress.', '', $wpseo_frontend->canonical( false ))),
	        'yoast_wpseo_opengraph-title' => get_post_meta($post_ID, '_yoast_wpseo_opengraph-title', true),
	        'yoast_wpseo_opengraph-description' => get_post_meta($post_ID, '_yoast_wpseo_opengraph-description', true),
	        'yoast_wpseo_opengraph-image' => get_post_meta($post_ID, '_yoast_wpseo_opengraph-image', true),
	        'yoast_wpseo_twitter-title' => get_post_meta($post_ID, '_yoast_wpseo_twitter-title', true),
	        'yoast_wpseo_twitter-description' => get_post_meta($post_ID, '_yoast_wpseo_twitter-description', true),
	        'yoast_wpseo_twitter-image' => get_post_meta($post_ID, '_yoast_wpseo_twitter-image', true)
	    );

	    wp_reset_query();

	}
    return $yoastMeta;
}

add_action('after_setup_theme', 'yoast_api');

function yoast_api() {
	if ( class_exists( 'WPSEO_Frontend' ) ) {
		class WPSEO_Frontend_To_REST_API extends WPSEO_Frontend {
			public static function get_instance() {
				if ( ! ( self::$instance instanceof self ) ) {
					self::$instance = new self();
				}
				return self::$instance;
			}
		}
	}
}

?>
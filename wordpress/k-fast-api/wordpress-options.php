<?php

/**********/
/* Images */
/**********/

add_image_size('mobile', 575, 0, false);
add_image_size('tablet', 618, 0, false); // - 150 from header
add_image_size('laptop', 842, 0, false); // - 150 from header
add_image_size('desktop', 1080, 0, false); // - 150 from header
add_image_size('max', 2395, 0, false); // - 150 from header

remove_image_size('medium');
remove_image_size('medium_large');
remove_image_size('large');

add_theme_support( 'post-thumbnails' ); 


/*******/
/* SVG */
/*******/

function add_file_types_to_uploads($file_types){
$new_filetypes = array();
$new_filetypes['svg'] = 'image/svg+xml';
$file_types = array_merge($file_types, $new_filetypes );
return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');


/**************/
/* Post-types */
/**************/

add_post_type_support( 'page', 'excerpt' );

function create_posttypes() {
	$labels = array(
	  'name'                => __('Artiklar', 'k-fast-api'),
	  'singular_name'       => __('Artikel', 'k-fast-api')
	);
	$args = array(
	  'labels'              => $labels,
	  'public'              => true,
	  'has_archive'         => true,
	  'rewrite' 			=> array('slug' => 'inspiration/%category%'),
	  'taxonomies'  		=> array( 'category' ),
	  'supports' 			=> array( 'title', 'excerpt', 'editor', 'thumbnail'  )
	);
	register_post_type( 'article', $args );


	/*$labels = array(
	  'name'                => __('FAQ', 'k-fast-api'),
	  'singular_name'       => __('FAQ', 'k-fast-api')
	);
	$args = array(
	  'labels'              => $labels,
	  'public'              => true,
	  'has_archive'         => true,
	  'taxonomies'  		=> array( 'faq_category' ),
	  'supports' 			=> array( 'title', 'editor' )
	);
	register_post_type( 'faq', $args );

	register_taxonomy('faq_category', 'faq', array('hierarchical' => true));*/


	/*$labels = array(
	  'name'                => __('Jobb', 'k-fast-api'),
	  'singular_name'       => __('Jobb', 'k-fast-api')
	);
	$args = array(
	  'labels'              => $labels,
	  'public'              => true,
	  'has_archive'         => true,
	  'supports' 			=> array( 'title', 'excerpt', 'editor', 'thumbnail'  )
	);
	register_post_type( 'job', $args );*/

	$labels = array(
	  'name'                => __('Anställda', 'k-fast-api'),
	  'singular_name'       => __('Anställd', 'k-fast-api')
	);
	$args = array(
	  'labels'              => $labels,
	  'public'              => true,
	  'has_archive'         => true,
	  'supports' 			=> array( 'title', 'thumbnail'  )
	);
	register_post_type( 'employee', $args );


}
add_action( 'init', 'create_posttypes' );


function custom_post_link( $post_link, $id = 0 ){
    $post = get_post($id);  
    if ( is_object( $post ) ){
        $terms = wp_get_object_terms( $post->ID, 'category' );
        if( $terms ){
            return str_replace( '%category%' , $terms[0]->slug , $post_link );
        }
    }
    return $post_link;  
}
add_filter( 'post_type_link', 'custom_post_link', 1, 3 );

?>
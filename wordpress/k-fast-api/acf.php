<?php

/***********/
/*   ACF   */
/***********/

if (function_exists('acf_add_options_page')) {
	acf_add_options_page();	
}

if( function_exists('acf_add_options_sub_page') ) {

	acf_add_options_sub_page('Header');
	acf_add_options_sub_page('Footer');
	acf_add_options_sub_page('Other');
	
}


function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyBvEsDVVMOsbVlWCTPXppjfUZHSlQxUmDM');
}

add_action('acf/init', 'my_acf_init');


function wprc_add_acf_posts_endpoint( $allowed_endpoints ) {
    if ( !isset( $allowed_endpoints[ 'core' ] )) {

    	$allowed_endpoints['core'] = array();

    	$endpoints = array('page', 'cpt', 'options', 'sitemap');

    	foreach ($endpoints as $endpoint) {
    		if(!in_array( $endpoint, $allowed_endpoints[ 'core' ] ) ) {
    			 $allowed_endpoints[ 'core' ][] = $endpoint;
    		}
    	}
    } 

    return $allowed_endpoints;
}

add_filter( 'wp_rest_cache/allowed_endpoints', 'wprc_add_acf_posts_endpoint', 10, 1);


?>
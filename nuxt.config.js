module.exports = {
  mode: 'universal',
  telemetry: false,
  debug: true,
  server: {
    host: '0.0.0.0',
    port: '3000'
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'pkg.name',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.jpg' }
    ],
    script: [

    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '@/assets/bootstrap-config.scss'
  ],

  components: true,

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/maps', ssr: false },
    { src: '~/plugins/inViewportDirective.js', ssr: false },
    { src: '~/plugins/globalFunctions.js' },
    { src: '~/plugins/axios.js' },
    { src: '~/plugins/basePlaceholder.js' }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://bootstrap-vue.js.org/docs/
    '@nuxtjs/svg-sprite',
    'bootstrap-vue/nuxt',
    ['@nuxtjs/recaptcha', {
      language: 'en',
      siteKey: '6LeKdr0UAAAAAKWtXjW6_7_iigsHrIjwn9BYYHgs', // Site key for requests
      version: 2 // Version
    }],

    '@nuxtjs/redirect-module',
    ['nuxt-i18n', {
      seo: false,
      detectBrowserLanguage: false,
      locales: [
        { code: 'sv', iso: 'sv-SE', name: 'Svenska', file: 'sv-SE.js' },
        { code: 'en', iso: 'en-US', name: 'English', file: 'en-US.js' }
      ],
      lazy: true,
      langDir: 'lang/',
      defaultLocale: 'sv',
      strategy: 'prefix_except_default',
      parsePages: false,
      pages: {
        '_page/index': {
          en: '/:page'
        },

        'inspiration/index': {
          en: '/inspiration'
        },
        'inspiration/_category': {
          en: '/inspiration/:category'
        },
        'inspiration/_category/_post': {
          en: '/inspiration/:category/:post'
        },

        'for-investerare/index': {
          en: '/investors'
        },

        'for-investerare/press/index': {
          en: '/investors/press-releases'
        },
        'for-investerare/press/_page': {
          en: '/investors/press-releases/:page'
        },
        'for-investerare/finansiella-rapporter': {
          en: '/investors/financial-reports'
        },
        'for-investerare/aktien': {
          en: '/investors/the-share'
        },
        'for-investerare/borsnotering': {
          en: '/investors/ipo'
        },
        'for-investerare/finansiell-kalender': {
          en: '/investors/financial-calendar'
        },

        'for-investerare/finansiella-mal': {
          en: '/investors/financial-objectives'
        },
        'for-investerare/presentation': {
          en: '/investors/presentations'
        },
        'for-investerare/risker-och-riskhantering': {
          en: '/investors/risks-and-risk-management'
        },
        'for-investerare/avstamningar-och-definitioner': {
          en: '/investors/reconciliation-table-and-definitions'
        },

        'for-investerare/_page': {
          en: '/investors/:page'
        },

        'bolagsstyrning/index': {
          en: '/corporate-governance'
        },
        'bolagsstyrning/styrelse': {
          en: '/corporate-governance/board-of-directors'
        },
        'bolagsstyrning/koncernledning': {
          en: '/corporate-governance/ceo-and-group-management'
        },
        'bolagsstyrning/revisor': {
          en: '/corporate-governance/auditors'
        },
        'bolagsstyrning/bolagsordning': {
          en: '/corporate-governance/articles-of-association'
        },
        'bolagsstyrning/bolagsstamma': {
          en: '/corporate-governance/general-meetings'
        },
        'bolagsstyrning/ersattning': {
          en: '/corporate-governance/remuneration'
        },
        'bolagsstyrning/interkontroll': {
          en: '/corporate-governance/internal-control'
        },
        'bolagsstyrning/rapport': {
          en: '/corporate-governance/report'
        },
        'bolagsstyrning/valberedning': {
          en: '/corporate-governance/nomination-committee'
        },
        'bolagsstyrning/styrelseutskott': {
          en: '/corporate-governance/board-committees'
        },
        'hallbarhet/index': {
          en: '/sustainability'
        },
        'detta-ar-k-fastigheter/index': {
          en: '/this-is-k-fastigheter'
        }

      },
      vueI18n: {
        fallbackLocale: 'sv'
      }
    }],
    '@nuxtjs/sitemap',
    ['nuxt-cookie-control', {
      text: {
        locale: {
          en: {
            barTitle: 'Cookies',
            barDescription: 'We use our own cookies and third-party cookies so that we can show you this website and better understand how you use it, with a view to improving the services we offer. If you continue browsing, we consider that you have accepted the cookies.',
            acceptAll: 'Accept all',
            declineAll: 'Delete all',
            manageCookies: 'Manage cookies',
            unsaved: 'You have unsaved settings',
            close: 'Close',
            save: 'Save',
            necessary: 'Necessary cookies',
            optional: 'Optional cookies'
          },

          de: {
            barTitle: 'Cookies',
            barDescription: 'Vi på K-Fastigheter använder Cookies på hemsidan för att ge dig bästa möjliga upplevelse. Du kan läsa mer om vår personuppgifts- och cookiepolicy ',
            acceptAll: 'Jag godkänner alla',
            declineAll: 'Radera alla',
            manageCookies: 'Cookieinställningar',
            unsaved: 'Det finns sparade ändringar',
            close: 'Stäng',
            save: 'Spara',
            necessary: 'Nödvändiga cookies',
            optional: 'Extra cookies'
          }
        }
      }
    }]
  ],

  redirect: [
    {
      from: (from, req) => {
        console.log(req, from)
        return false
      },
      to: '/hyresfastigheter'
    },
    { from: '^/ledigt/?$', to: '/hyresfastigheter' },
    { from: '^/vara-fastigheter', to: '/felanmalan' },
    { from: '^/att-bo-hos-oss/(.*)$', to: '/om-oss/att-bo-hos-oss' },
    { from: '^/intresseanmalan', to: '/soker-du-bostad' }
  ],

  cookies: {
    necessary: [
      {
        name: 'Används för att kontrollera om användaren har accepterat cookies',
        // if multilanguage
        description: {
          en: 'Used for cookie control.',
          de: ''
        },
        cookies: ['cookie_control_consent', 'cookie_control_enabled_cookies']
      }
    ],
    optional: [
      {
        name: 'Google Analitycs',
        // if multilanguage
        description: {
          en: 'Google GTM is used to create a better experience for the user',
          de: 'Google GTM används för att skapa en bättre upplevelse för användaren'
        },
        src: 'https://www.googletagmanager.com/gtag/js?id=GTM-PC6FBDF',
        async: true,
        cookies: ['ga', 'gat', 'gat_UA-100070383-1', 'gid'],
        accepted: () => {
          window.dataLayer = window.dataLayer || []
          window.dataLayer.push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
          })
        },
        declined: () => {
        }
      }
    ]
  },

  sitemap: {
    gzip: true,
    routes: [
      '/inspiration/holknekt-hittar-hem',
      '/inspiration/bygglov',
      '/inspiration/aktuellt',
      '/inspiration/forvarv',
      '/om-oss/att-bo-hos-oss',
      '/om-oss/miljo-kvalitet',
      '/cookiepolicy',
      '/integritetspolicy'
    ]
  },

  bootstrapVue: {
    bootstrapCSS: false, // or `css`
    bootstrapVueCSS: false // or `bvCSS`
  },
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    proxyHeaders: false
  },

  router: {
    middleware: ['getMeta', 'setMeta', 'getArchive'],
    scrollBehavior: async (to, from, savedPosition) => {
      if (to.name.indexOf('hyresfastigheter-type') === 0) {
        return window.scrollTo(0, window.innerHeight * 0.8)
      }

      window.dataLayer = window.dataLayer || []

      setTimeout(() => {
        window.dataLayer.push(to.gtm || { event: 'nuxtRoute', pageType: 'PageView', pageUrl: to.fullPath, routeName: to.name })
      }, 0)

      if (savedPosition) {
        return savedPosition
      }

      const findEl = async (hash, x) => {
        return document.querySelector(hash) ||
          new Promise((resolve, reject) => {
            if (x > 50) {
              return resolve()
            }
            setTimeout(() => { resolve(findEl(hash, ++x || 1)) }, 100)
          })
      }

      if (to.hash) {
        const el = await findEl(to.hash)
        if ('scrollBehavior' in document.documentElement.style) {
          return window.scrollTo({ top: el.offsetTop, behavior: 'smooth' })
        } else {
          return window.scrollTo(0, el.offsetTop)
        }
      }

      return { x: 0, y: 0 }
    }
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      config.resolve.alias.vue$ = 'vue/dist/vue.esm.js'
      // Run ESLint on save

      config.node = {
        fs: 'empty',
        net: 'empty',
        tls: 'empty'
      }

      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          options: 'fix',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

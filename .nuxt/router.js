import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL } from '@nuxt/ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _3ea7aaf8 = () => interopDefault(import('../pages/bolagsstyrning/index.vue' /* webpackChunkName: "pages/bolagsstyrning/index" */))
const _b0732dd6 = () => interopDefault(import('../pages/detta-ar-k-fastigheter/index.vue' /* webpackChunkName: "pages/detta-ar-k-fastigheter/index" */))
const _4f725168 = () => interopDefault(import('../pages/for-investerare/index.vue' /* webpackChunkName: "pages/for-investerare/index" */))
const _4018abf9 = () => interopDefault(import('../pages/hallbarhet/index.vue' /* webpackChunkName: "pages/hallbarhet/index" */))
const _6b98a7b8 = () => interopDefault(import('../pages/inspiration/index.vue' /* webpackChunkName: "pages/inspiration/index" */))
const _7f6d7a0d = () => interopDefault(import('../pages/bolagsstyrning/bolagsordning.vue' /* webpackChunkName: "pages/bolagsstyrning/bolagsordning" */))
const _6e5e304e = () => interopDefault(import('../pages/bolagsstyrning/bolagsstamma.vue' /* webpackChunkName: "pages/bolagsstyrning/bolagsstamma" */))
const _0fed140e = () => interopDefault(import('../pages/bolagsstyrning/ersattning.vue' /* webpackChunkName: "pages/bolagsstyrning/ersattning" */))
const _77cf4a29 = () => interopDefault(import('../pages/bolagsstyrning/interkontroll.vue' /* webpackChunkName: "pages/bolagsstyrning/interkontroll" */))
const _632dbc3e = () => interopDefault(import('../pages/bolagsstyrning/koncernledning.vue' /* webpackChunkName: "pages/bolagsstyrning/koncernledning" */))
const _4b461f56 = () => interopDefault(import('../pages/bolagsstyrning/revisor.vue' /* webpackChunkName: "pages/bolagsstyrning/revisor" */))
const _ad1ba186 = () => interopDefault(import('../pages/bolagsstyrning/styrelse.vue' /* webpackChunkName: "pages/bolagsstyrning/styrelse" */))
const _9fea962a = () => interopDefault(import('../pages/bolagsstyrning/styrelseutskott.vue' /* webpackChunkName: "pages/bolagsstyrning/styrelseutskott" */))
const _c5f3d22a = () => interopDefault(import('../pages/bolagsstyrning/valberedning.vue' /* webpackChunkName: "pages/bolagsstyrning/valberedning" */))
const _7463d862 = () => interopDefault(import('../pages/for-investerare/aktien.vue' /* webpackChunkName: "pages/for-investerare/aktien" */))
const _48b257d6 = () => interopDefault(import('../pages/for-investerare/avstamningar-och-definitioner.vue' /* webpackChunkName: "pages/for-investerare/avstamningar-och-definitioner" */))
const _3c7f1f6c = () => interopDefault(import('../pages/for-investerare/borsnotering.vue' /* webpackChunkName: "pages/for-investerare/borsnotering" */))
const _49c40cbe = () => interopDefault(import('../pages/for-investerare/finansiell-kalender.vue' /* webpackChunkName: "pages/for-investerare/finansiell-kalender" */))
const _2695546a = () => interopDefault(import('../pages/for-investerare/finansiella-mal.vue' /* webpackChunkName: "pages/for-investerare/finansiella-mal" */))
const _9a159dfc = () => interopDefault(import('../pages/for-investerare/finansiella-rapporter.vue' /* webpackChunkName: "pages/for-investerare/finansiella-rapporter" */))
const _73ae2bf4 = () => interopDefault(import('../pages/for-investerare/presentation.vue' /* webpackChunkName: "pages/for-investerare/presentation" */))
const _4a3fcc1c = () => interopDefault(import('../pages/for-investerare/press/index.vue' /* webpackChunkName: "pages/for-investerare/press/index" */))
const _2e713897 = () => interopDefault(import('../pages/for-investerare/risker-och-riskhantering.vue' /* webpackChunkName: "pages/for-investerare/risker-och-riskhantering" */))
const _99fdde90 = () => interopDefault(import('../pages/for-investerare/press/_page.vue' /* webpackChunkName: "pages/for-investerare/press/_page" */))
const _b12e20d8 = () => interopDefault(import('../pages/bolagsstyrning/_page.vue' /* webpackChunkName: "pages/bolagsstyrning/_page" */))
const _8f98d3f8 = () => interopDefault(import('../pages/for-investerare/_page.vue' /* webpackChunkName: "pages/for-investerare/_page" */))
const _d225231c = () => interopDefault(import('../pages/inspiration/_category/index.vue' /* webpackChunkName: "pages/inspiration/_category/index" */))
const _0cd90ec2 = () => interopDefault(import('../pages/inspiration/_category/_post.vue' /* webpackChunkName: "pages/inspiration/_category/_post" */))
const _13c54f89 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _535aaae8 = () => interopDefault(import('../pages/_page/index.vue' /* webpackChunkName: "pages/_page/index" */))
const _93fe95d6 = () => interopDefault(import('../pages/_page/_post.vue' /* webpackChunkName: "pages/_page/_post" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/bolagsstyrning",
    component: _3ea7aaf8,
    name: "bolagsstyrning___sv"
  }, {
    path: "/en/corporate-governance",
    component: _3ea7aaf8,
    name: "bolagsstyrning___en"
  }, {
    path: "/detta-ar-k-fastigheter",
    component: _b0732dd6,
    name: "detta-ar-k-fastigheter___sv"
  }, {
    path: "/en/this-is-k-fastigheter",
    component: _b0732dd6,
    name: "detta-ar-k-fastigheter___en"
  }, {
    path: "/for-investerare",
    component: _4f725168,
    name: "for-investerare___sv"
  }, {
    path: "/en/investors",
    component: _4f725168,
    name: "for-investerare___en"
  }, {
    path: "/hallbarhet",
    component: _4018abf9,
    name: "hallbarhet___sv"
  }, {
    path: "/en/sustainability",
    component: _4018abf9,
    name: "hallbarhet___en"
  }, {
    path: "/inspiration",
    component: _6b98a7b8,
    name: "inspiration___sv"
  }, {
    path: "/en/inspiration",
    component: _6b98a7b8,
    name: "inspiration___en"
  }, {
    path: "/bolagsstyrning/bolagsordning",
    component: _7f6d7a0d,
    name: "bolagsstyrning-bolagsordning___sv"
  }, {
    path: "/en/corporate-governance/articles-of-association",
    component: _7f6d7a0d,
    name: "bolagsstyrning-bolagsordning___en"
  }, {
    path: "/bolagsstyrning/bolagsstamma",
    component: _6e5e304e,
    name: "bolagsstyrning-bolagsstamma___sv"
  }, {
    path: "/en/corporate-governance/general-meetings",
    component: _6e5e304e,
    name: "bolagsstyrning-bolagsstamma___en"
  }, {
    path: "/bolagsstyrning/ersattning",
    component: _0fed140e,
    name: "bolagsstyrning-ersattning___sv"
  }, {
    path: "/en/corporate-governance/remuneration",
    component: _0fed140e,
    name: "bolagsstyrning-ersattning___en"
  }, {
    path: "/bolagsstyrning/interkontroll",
    component: _77cf4a29,
    name: "bolagsstyrning-interkontroll___sv"
  }, {
    path: "/en/corporate-governance/internal-control",
    component: _77cf4a29,
    name: "bolagsstyrning-interkontroll___en"
  }, {
    path: "/bolagsstyrning/koncernledning",
    component: _632dbc3e,
    name: "bolagsstyrning-koncernledning___sv"
  }, {
    path: "/en/corporate-governance/ceo-and-group-management",
    component: _632dbc3e,
    name: "bolagsstyrning-koncernledning___en"
  }, {
    path: "/bolagsstyrning/revisor",
    component: _4b461f56,
    name: "bolagsstyrning-revisor___sv"
  }, {
    path: "/en/corporate-governance/auditors",
    component: _4b461f56,
    name: "bolagsstyrning-revisor___en"
  }, {
    path: "/bolagsstyrning/styrelse",
    component: _ad1ba186,
    name: "bolagsstyrning-styrelse___sv"
  }, {
    path: "/en/corporate-governance/board-of-directors",
    component: _ad1ba186,
    name: "bolagsstyrning-styrelse___en"
  }, {
    path: "/bolagsstyrning/styrelseutskott",
    component: _9fea962a,
    name: "bolagsstyrning-styrelseutskott___sv"
  }, {
    path: "/en/corporate-governance/board-committees",
    component: _9fea962a,
    name: "bolagsstyrning-styrelseutskott___en"
  }, {
    path: "/bolagsstyrning/valberedning",
    component: _c5f3d22a,
    name: "bolagsstyrning-valberedning___sv"
  }, {
    path: "/en/corporate-governance/nomination-committee",
    component: _c5f3d22a,
    name: "bolagsstyrning-valberedning___en"
  }, {
    path: "/for-investerare/aktien",
    component: _7463d862,
    name: "for-investerare-aktien___sv"
  }, {
    path: "/en/investors/the-share",
    component: _7463d862,
    name: "for-investerare-aktien___en"
  }, {
    path: "/for-investerare/avstamningar-och-definitioner",
    component: _48b257d6,
    name: "for-investerare-avstamningar-och-definitioner___sv"
  }, {
    path: "/en/investors/reconciliation-table-and-definitions",
    component: _48b257d6,
    name: "for-investerare-avstamningar-och-definitioner___en"
  }, {
    path: "/for-investerare/borsnotering",
    component: _3c7f1f6c,
    name: "for-investerare-borsnotering___sv"
  }, {
    path: "/en/investors/ipo",
    component: _3c7f1f6c,
    name: "for-investerare-borsnotering___en"
  }, {
    path: "/for-investerare/finansiell-kalender",
    component: _49c40cbe,
    name: "for-investerare-finansiell-kalender___sv"
  }, {
    path: "/en/investors/financial-calendar",
    component: _49c40cbe,
    name: "for-investerare-finansiell-kalender___en"
  }, {
    path: "/for-investerare/finansiella-mal",
    component: _2695546a,
    name: "for-investerare-finansiella-mal___sv"
  }, {
    path: "/en/investors/financial-objectives",
    component: _2695546a,
    name: "for-investerare-finansiella-mal___en"
  }, {
    path: "/for-investerare/finansiella-rapporter",
    component: _9a159dfc,
    name: "for-investerare-finansiella-rapporter___sv"
  }, {
    path: "/en/investors/financial-reports",
    component: _9a159dfc,
    name: "for-investerare-finansiella-rapporter___en"
  }, {
    path: "/for-investerare/presentation",
    component: _73ae2bf4,
    name: "for-investerare-presentation___sv"
  }, {
    path: "/en/investors/presentations",
    component: _73ae2bf4,
    name: "for-investerare-presentation___en"
  }, {
    path: "/for-investerare/press",
    component: _4a3fcc1c,
    name: "for-investerare-press___sv"
  }, {
    path: "/en/investors/press-releases",
    component: _4a3fcc1c,
    name: "for-investerare-press___en"
  }, {
    path: "/for-investerare/risker-och-riskhantering",
    component: _2e713897,
    name: "for-investerare-risker-och-riskhantering___sv"
  }, {
    path: "/en/investors/risks-and-risk-management",
    component: _2e713897,
    name: "for-investerare-risker-och-riskhantering___en"
  }, {
    path: "/for-investerare/press/:page",
    component: _99fdde90,
    name: "for-investerare-press-page___sv"
  }, {
    path: "/en/investors/press-releases/:page",
    component: _99fdde90,
    name: "for-investerare-press-page___en"
  }, {
    path: "/bolagsstyrning/:page",
    component: _b12e20d8,
    name: "bolagsstyrning-page___sv"
  }, {
    path: "/en/bolagsstyrning/:page",
    component: _b12e20d8,
    name: "bolagsstyrning-page___en"
  }, {
    path: "/for-investerare/:page?",
    component: _8f98d3f8,
    name: "for-investerare-page___sv"
  }, {
    path: "/en/investors/:page",
    component: _8f98d3f8,
    name: "for-investerare-page___en"
  }, {
    path: "/inspiration/:category",
    component: _d225231c,
    name: "inspiration-category___sv"
  }, {
    path: "/en/inspiration/:category",
    component: _d225231c,
    name: "inspiration-category___en"
  }, {
    path: "/inspiration/:category/:post",
    component: _0cd90ec2,
    name: "inspiration-category-post___sv"
  }, {
    path: "/en/inspiration/:category/:post",
    component: _0cd90ec2,
    name: "inspiration-category-post___en"
  }, {
    path: "/",
    component: _13c54f89,
    name: "index___sv"
  }, {
    path: "/en/",
    component: _13c54f89,
    name: "index___en"
  }, {
    path: "/:page",
    component: _535aaae8,
    name: "page___sv"
  }, {
    path: "/en/:page",
    component: _535aaae8,
    name: "page___en"
  }, {
    path: "/:page/:post",
    component: _93fe95d6,
    name: "page-post___sv"
  }, {
    path: "/en/:page/:post",
    component: _93fe95d6,
    name: "page-post___en"
  }],

  fallback: false
}

function decodeObj(obj) {
  for (const key in obj) {
    if (typeof obj[key] === 'string') {
      obj[key] = decodeURIComponent(obj[key])
    }
  }
}

export function createRouter () {
  const router = new Router(routerOptions)

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    const r = resolve(to, current, append)
    if (r && r.resolved && r.resolved.query) {
      decodeObj(r.resolved.query)
    }
    return r
  }

  return router
}

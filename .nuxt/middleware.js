const middleware = {}

middleware['getArchive'] = require('../middleware/getArchive.js')
middleware['getArchive'] = middleware['getArchive'].default || middleware['getArchive']

middleware['getMeta'] = require('../middleware/getMeta.js')
middleware['getMeta'] = middleware['getMeta'].default || middleware['getMeta']

middleware['setMeta'] = require('../middleware/setMeta.js')
middleware['setMeta'] = middleware['setMeta'].default || middleware['setMeta']

export default middleware

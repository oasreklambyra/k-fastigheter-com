export const state = () => ({
  pages: { sv: {}, en: {} },
  menus: { sv: {}, en: {} },
  pagesPreview: { sv: {}, en: {} },
  pagesMetas: { sv: {}, en: {} },
  highlight: { sv: false, en: false },
  about: { sv: false, en: false },
  warning: { sv: false, en: false },
  cpt: { sv: {}, en: {} },
  tax: { sv: {}, en: {} },
  options: { sv: false, en: false },
  sitemap: { sv: false, en: false },
  wordpress: 'https://wordpress.k-fastigheter.com/wp-json/',
  siteBase: (process.env.NODE_ENV === 'production') ? 'https://www.k-fastigheter.com' : 'http://localhost:3000'

})

export const mutations = {

  getPage(state, data) {
    const temporaryHolder = state.pages
    state.pages = [] // Updates the structure
    temporaryHolder[state.i18n.locale][data.path] = data.data
    state.pages = temporaryHolder
  },
  getMenus(state, data) {
    const temporaryHolder = state.menus
    state.menus = [] // Updates the structure
    temporaryHolder[state.i18n.locale][data.menu] = data.data
    state.menus = temporaryHolder
  },
  getPagePreview(state, data) {
    const temporaryHolder = state.pagesPreview
    state.pagesPreview = [] // Updates the structure
    temporaryHolder[state.i18n.locale][data.path] = data.data
    state.pagesPreview = temporaryHolder
  },
  getPageMeta(state, data) {
    const temporaryHolder = state.pagesMetas
    state.pagesMetas = [] // Updates the structure
    temporaryHolder[state.i18n.locale][data.path] = data.data
    state.pagesMetas = temporaryHolder
  },
  getSitemap(state, data) {
    state.sitemap[state.i18n.locale] = data
  },
  getOptions(state, data) {
    state.options[state.i18n.locale] = data.data
  },
  getHighlight(state, data) {
    state.highlight[state.i18n.locale] = data
  },
  getAbout(state, data) {
    state.about[state.i18n.locale] = data
  },
  getWarning(state, data) {
    state.warning[state.i18n.locale] = data
  },
  getCpt(state, data) {
    const temporaryHolder = state.cpt
    state.cpt = [] // Updates the structure
    temporaryHolder[state.i18n.locale][data.path] = data.data
    state.cpt = temporaryHolder
  },
  getTax(state, data) {
    const temporaryHolder = state.tax
    state.tax = [] // Updates the structure
    temporaryHolder[state.i18n.locale][data.type] = data.data
    state.tax = temporaryHolder
  },

  /* Filter */

  setLocation(state, data) {
    const temporaryHolder = state.filter.location
    state.filter.location[state.i18n.locale] = [] // Updates the structure

    temporaryHolder[state.i18n.locale] = data
    state.filter.location = temporaryHolder
  },

  setArea(state, data) {
    const temporaryHolder = state.filter.area
    state.filter.area[state.i18n.locale] = [] // Updates the structure

    temporaryHolder[state.i18n.locale] = data
    state.filter.area = temporaryHolder
  },

  setSize(state, data) {
    const temporaryHolder = state.filter.size
    state.filter.size[state.i18n.locale] = [] // Updates the structure

    temporaryHolder[state.i18n.locale] = data
    state.filter.size = temporaryHolder
  }
}

export const getters = {
  getPage(state) {
    return state.pages[state.i18n.locale]
  },
  getMenus(state) {
    return state.menus[state.i18n.locale]
  },
  getPagePreview(state) {
    return state.pagesPreview[state.i18n.locale]
  },
  getPageMeta(state) {
    return state.pagesMetas[state.i18n.locale]
  },
  getSitemap(state) {
    return state.sitemap[state.i18n.locale]
  },
  getOptions(state) {
    return state.options[state.i18n.locale]
  },
  getHighlight(state) {
    return state.highlight[state.i18n.locale]
  },
  getAbout(state) {
    return state.about[state.i18n.locale]
  },
  getWarning(state) {
    return state.warning[state.i18n.locale]
  },
  getCpt(state) {
    return state.cpt[state.i18n.locale]
  },
  getTax(state) {
    return state.tax[state.i18n.locale]
  },

  /* Filter */

  getLocation(state) {
    return state.filter.location[state.i18n.locale]
  },
  getArea(state) {
    return state.filter.area[state.i18n.locale]
  },
  getSize(state) {
    return state.filter.size[state.i18n.locale]
  }
}

export const actions = {
  nuxtServerInit({ dispatch }, { app }) {
    dispatch('cision/setLocale', app.i18n.locale)
  },

  async getPage(context, payload) {
    let lang = context.state.i18n.locale
    if (payload.lang) {
      lang = payload.lang
    }
    const type = (payload.postType) ? payload.postType : 'any'

    const res = await this.$axios.get(context.state.wordpress + 'core/page?slug=' + payload.path + '&type=' + type + '&lang=' + lang)
    context.commit('getPage', {
      path: res.data[0].post_type + '/' + payload.path,
      data: res.data[0]
    })
  },

  async getSitemap(context, payload) {
    let lang = context.state.i18n.locale
    if (payload.lang) {
      lang = payload.lang
    }

    const res = await this.$axios.get(context.state.wordpress + 'core/sitemap' + '?lang=' + lang)
    // const res = await this.$axios.get('https://k-fastigheter.com/sitemap.xml')
    context.commit('getSitemap', res.data)
  },

  async getOptions(context, payload) {
    let lang = context.state.i18n.locale
    if (payload.lang) {
      lang = payload.lang
    }
    const res = await this.$axios.get(context.state.wordpress + 'core/options' + '?lang=' + lang)

    context.commit('getHighlight', res.data.highlight)

    context.commit('getAbout', res.data.about)

    context.commit('getMenus', {
      data: res.data.small_menu,
      menu: 'small_menu'
    })

    context.commit('getMenus', {
      data: res.data.big_menu,
      menu: 'big_menu'
    })

    context.commit('getMenus', {
      data: { footer_menu: res.data.footer_menu, footer_description: res.data.footer_description },
      menu: 'footer_menu'
    })

    context.commit('getMenus', {
      data: { header_menu: res.data.header_menu, header_description: res.data.header_description },
      menu: 'header_menu'
    })

    context.commit('getWarning', {
      display_notice: res.data.display_notice,
      notice_text: res.data.notice_text,
      background: res.data.background_color
    })

    context.commit('getOptions', {
      data: res.data
    })
  },

  async getCpt(context, payload) {
    let lang = context.state.i18n.locale
    if (payload.lang) {
      lang = payload.lang
    }

    const res = await this.$axios.get(context.state.wordpress + 'core/cpt?type=' + payload.postType + '&lang=' + lang)
    const jobs = []

    Object.keys(res.data).forEach((key) => {
      context.commit('getPagePreview', {
        path: res.data[key].post_type + '/' + res.data[key].post_name,
        data: res.data[key]
      })

      jobs.push(res.data[key].post_name)
    })

    context.commit('getCpt', {
      path: payload.postType,
      data: jobs
    })
  }

}

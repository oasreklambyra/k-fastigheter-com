export const state = () => ({
  cisionSummary: { sv: false, en: false },
  cisionSingle: { sv: {}, en: {} },
  cisionRes: {
    share: { sv: {}, en: {} },
    performance: { sv: {}, en: {} },
    orderbook: { sv: {}, en: {} }
  },
  uniqueIdentifiers: {
    share: {
      sv: '840CADBB98C544E99B0B959B6158D624',
      en: 'CB4BC00A17C74E36B28FC8806054B6F7'
    },
    performance: {
      sv: '2E63BAD33C30414D80574E9E43D0234B',
      en: '2E63BAD33C30414D80574E9E43D0234B'
    },
    orderbook: {
      sv: '5CF3662A8D444AE293A25FF22AAD74D6',
      en: '5CF3662A8D444AE293A25FF22AAD74D6'
    }
  },
  i18n: {
    locale: 'sv'
  },
  cisionBase: 'https://publish.ne.cision.com/papi/'
})

export const mutations = {
  setLocale(state, data) {
    state.i18n.locale = data
  },

  getCisionSummary(state, data) {
    const temporaryHolder = state.cisionSummary
    state.cisionSummary[state.i18n.locale] = [] // Updates the structure

    temporaryHolder[state.i18n.locale] = data.data
    state.cisionSummary = temporaryHolder
  },
  getCisionSingle(state, data) {
    const temporaryHolder = state.cisionSingle
    state.cisionSingle[state.i18n.locale] = {} // Updates the structure

    temporaryHolder[state.i18n.locale][data.id] = data.data
    state.cisionSingle = temporaryHolder
  },

  getCisionShare(state, data) {
    const temporaryHolder = state.cisionRes.share
    state.cisionRes.share[state.i18n.locale] = [] // Updates the structure

    temporaryHolder[state.i18n.locale] = data
    state.cisionRes.share = temporaryHolder
  },

  getCisionPerformance(state, data) {
    const temporaryHolder = state.cisionRes.performance
    state.cisionRes.performance[state.i18n.locale] = [] // Updates the structure

    temporaryHolder[state.i18n.locale] = data
    state.cisionRes.performance = temporaryHolder
  },

  getCisionOrderbook(state, data) {
    const temporaryHolder = state.cisionRes.orderbook
    state.cisionRes.orderbook[state.i18n.locale] = [] // Updates the structure

    temporaryHolder[state.i18n.locale] = data
    state.cisionRes.orderbook = temporaryHolder
  }
}

export const getters = {

  getCisionSummary(state) {
    return state.cisionSummary[state.i18n.locale]
  },
  getCisionSingle(state) {
    return state.cisionSingle[state.i18n.locale]
  },
  getCisionShare(state) {
    return state.cisionRes.share[state.i18n.locale]
  },
  getCisionPerformance(state) {
    return state.cisionRes.performance[state.i18n.locale]
  },
  getCisionOrderbook(state) {
    return state.cisionRes.orderbook[state.i18n.locale]
  }
}

export const actions = {
  setLocale(context, payload) {
    context.commit('setLocale', payload)
  },

  async getCisionSummary(context, payload) {
    let key = '0F97879C8EA546D586CFB7657A909A8E'

    if (context.state.i18n.locale === 'en') {
      key = 'B8B1AB51A8AA4F788075214D9B6EF9A0'
    }

    const res = await this.$axios.get('https://publish.ne.cision.com/papi/NewsFeed/' + key + '?format=json&detailLevel=medium&pageSize=9999999&pageIndex=1')

    /*
      "InformationType": "PRM", => Pressmedelande
      "InformationType": "RPT" => delårsrapport,
      "InformationType": "RDV", => årsredovisning
      "InformationType": "KMK", => bokslutskomite
      "InformationType": "INB", => bokslutskomite 481c97c5-0bd7-4aff-aca2-71ce9003b613
    */

    context.commit('getCisionSummary', {
      data: res.data
    })
  },

  async getCisionSingle(context, payload) {
    const res = await this.$axios.get('https://publish.ne.cision.com/papi/Release/' + payload.id + '?format=json')

    context.commit('getCisionSingle', {
      id: payload.id,
      data: res.data
    })
  },

  async getCisionShare(context, payload) {
    let lang = context.state.i18n.locale
    if (payload.lang) {
      lang = payload.lang
    }

    const key = context.state.uniqueIdentifiers.share[lang]
    const res = await this.$axios.get(context.state.cisionBase + 'Share/' + key + '?format=json')

    context.commit('getCisionShare', res.data)
  },

  async getCisionPerformance(context, payload) {
    let lang = context.state.i18n.locale
    if (payload.lang) {
      lang = payload.lang
    }

    const key = context.state.uniqueIdentifiers.performance[lang]
    const res = await this.$axios.get(context.state.cisionBase + 'Ticker/' + key + '/Performance?format=json')

    context.commit('getCisionPerformance', res.data)
  },

  async getCisionOrderbook(context, payload) {
    let lang = context.state.i18n.locale
    if (payload.lang) {
      lang = payload.lang
    }

    const key = context.state.uniqueIdentifiers.orderbook[lang]
    const res = await this.$axios.get(context.state.cisionBase + 'OrderBook/' + key + '?format=json')

    context.commit('getCisionOrderbook', res.data)
  },

  /* Filter */

  setLocation(context, payload) {
    context.commit('setLocation', payload.val)
  },
  setArea(context, payload) {
    context.commit('setArea', payload.val)
  },
  setSize(context, payload) {
    context.commit('setSize', payload.val)
  }
}
